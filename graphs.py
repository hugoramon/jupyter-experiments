


#How the non static device see the other devices
inverse_distances = distances.copy()
inverse_distances.reverse()

count_distances = len(distances)
#Define duration in seconds
duration = timedelta(seconds = interval * count_distances)
start = datetime.strptime(start_date, '%Y-%m-%d %H:%M:%S'); 
end = start + duration

#How they see the static devices the non static devices with time
#Matrix where they see the non_static device
dfs =  df1[(df1.ID2 == non_static_device[0]) & (df1.timestamp >= start) & ((df1.timestamp) <=end)]

#Matrix that represent how each static device see the non static device
d1 = dfs[dfs.ID1 == static_devices[0]['id']]
d2 = dfs[dfs.ID1 == static_devices[1]['id']]

fig = plt.plot()
plt.title("RSSI by time")
plt.xlabel('Milliseconds')
plt.ylabel('Rssi')
device1 = plt.scatter(d1['milliseconds'],d1['rssi'],label =static_devices[0]['name'])
device2 = plt.scatter(d2['milliseconds'],d2['rssi'], label ="Lenovo")
device2.set_label('OP3')
lenovo.set_label('Lenovo')
plt.legend()
plt.show()

#Calcultate rssi mean with distances

d1RD = {
    'distances' : [],
    'mean_rssi' : [],
    'max_rssi' : [],
}
d2RD = {
    'distances' : [],
    'mean_rssi' : [],
    'max_rssi' : [],
}
for i in range(0,len(distances)):
    start_point = start + timedelta(seconds = interval * i)
    end_point = start_point + timedelta(seconds = interval)
    #For device that start at a distance of 0 meters
    d1_interval = d1[(d1.timestamp >= start_point) & (d1.timestamp <= end_point)].rssi.unique().tolist()
    if(len(d1_interval) > 0):
        #Making the mean
        d1_mean = sum(d1_interval) / len(d1_interval)
        #Adding the value
        d1RD['distances'].append(distances[i])
        d1RD['mean_rssi'].append(d1_mean)
        d1RD['max_rssi'].append(max(d1_interval))

    #For device that start at a sitance of 10 meters
    d2_interval = d2[(d2.timestamp >= start_point) & (d2.timestamp <= end_point)].rssi.unique().tolist()
    if(len(d2_interval) > 0):
        d2_mean = sum(d2_interval) / len(d2_interval)
        #Adding the value
        d2RD['distances'].append(inverse_distances[i])
        d2RD['mean_rssi'].append(d2_mean)
        d2RD['max_rssi'].append(max(d2_interval))

df1RD = pd.DataFrame(d1RD,columns=['distances','mean_rssi','max_rssi'])
df2RD = pd.DataFrame(d2RD,columns=['distances','mean_rssi','max_rssi'])


#Mean rssi with distance
fig = plt.plot()
plt.title("Mean Rssi")
plt.xlabel('Distance')
plt.ylabel('Mean rssi')
op3 = plt.scatter(df1RD['distances'],df1RD['mean_rssi'],label ="Op3")
lenovo = plt.scatter(df2RD['distances'],df2RD['mean_rssi'], label ="Lenovo")
op3.set_label('OP3')
lenovo.set_label('Lenovo')
plt.legend()
plt.show()



#Max rssi with distance
fig = plt.plot()
plt.title("Max rssi")
plt.xlabel('Distance')
plt.ylabel('Max rssi')
op3 = plt.scatter(df1RD['distances'],df1RD['max_rssi'],label ="Op3")
lenovo = plt.scatter(df2RD['distances'],df2RD['max_rssi'], label ="Lenovo")
op3.set_label('OP3')
lenovo.set_label('Lenovo')
plt.legend()
plt.show()
